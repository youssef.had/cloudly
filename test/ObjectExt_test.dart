import 'package:flutter_test/flutter_test.dart';
import 'package:cloudly/Extensions/ObjectExt.dart';

main() {

	group('Object extension tests', () {
		test('check null object prop', () {
			String name = '';
			expect(name.isNull, false);
			//
			name = null;
			expect(name.isNull, true);
		});
	});
}