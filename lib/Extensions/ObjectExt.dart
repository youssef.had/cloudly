extension ObjectExt on Object {
	
	// DOC
	/// Check if the object is not null
	// SEALED
	bool get isNull {
		return this == null;
	}

	// DOC
	/// Check if the object is not null
	// SEALED
	bool get isNotNull {
		return ! this.isNull;
	}
}