import 'package:cloudly/Env/Colors.dart'
as Env;
import 'package:cloudly/Pages/HomePage.dart';
import 'package:flutter/material.dart';

class Cloudly extends StatelessWidget {


	// DOC
	/// This widget is the root of your application.
	/// 
	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			title: 'Flutter Demo',
			//
			// INFO Set the app theme
			theme: _appThemeData(),

			//
			// INFO Disable debug banner
			debugShowCheckedModeBanner: false,
			

			//
			// INFO Lanch page
			home: HomePage(),
			// home: DemoPage(title: 'Flutter Demo Home Page'),
		);
	}

	
	// DOC
	/// Set the app theme data
	///
	ThemeData _appThemeData() => 
		ThemeData(
			primaryColor: Env.Colors.MainColor,
		);
}