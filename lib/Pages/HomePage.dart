import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:cloudly/Pages/GroupsPage.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _page = 0;
  PageController _pageController;

  @override
  void initState() {
    _pageController = PageController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: _getNavigationBar(), body: _getContainers());
  }

  // DOC
  /// Get the home screen navigation bar
  // SEALED
  Widget _getNavigationBar() => BottomNavyBar(
      selectedIndex: _page,
      showElevation: true, // use this to remove appBar's elevation
      onItemSelected: (index) => setState(() {
            _page = index;
            _pageController.animateToPage(index,
                duration: Duration(milliseconds: 300), curve: Curves.ease);
          }),
      items: _getNavigationItem());

  // DOC
  /// Get the navigation bar items
  //
  List<BottomNavyBarItem> _getNavigationItem() => <BottomNavyBarItem>[
        BottomNavyBarItem(
            icon: Icon(Icons.widgets),
            title: Text('Groupes'),
            activeColor: Colors.lightBlue,
            textAlign: TextAlign.center),
        BottomNavyBarItem(
            icon: Icon(Icons.archive),
            title: Text('Archives'),
            activeColor: Colors.amber[700],
            textAlign: TextAlign.center),
        BottomNavyBarItem(
            icon: Icon(Icons.settings),
            title: Text('Settings'),
            activeColor: Colors.pink,
            textAlign: TextAlign.center)
      ];

  // DOC
  /// Get the containers of the home page
  //
  //  TODO Get the containers of the home page
  Widget _getContainers() => SizedBox.expand(
        child: PageView(
          controller: _pageController,
          onPageChanged: (index) {
            setState(() => _page = index);
          },
          children: <Widget>[
            GroupsPage(
              textColor: Colors.lightBlue,
            ),
            Container(
              color: Colors.red,
            ),
            Container(
              color: Colors.green,
            ),
          ],
        ),
      );
}
