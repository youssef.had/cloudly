import 'package:cloudly/Components/CustomAppBar.dart';
import 'package:cloudly/Env/Colors.dart'
as Env;
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class GroupsPage extends StatefulWidget {

	Color textColor;

	GroupsPage({this.textColor})
	{
		this.textColor = this.textColor ?? Colors.black87;
	}


	@override
	_GroupsPageState createState() => _GroupsPageState();
}

class _GroupsPageState extends State < GroupsPage > {
	@override
	Widget build(BuildContext context) {
		return Scaffold(

			backgroundColor: Env.Colors.MainBackgroundColor,
			appBar: _buildAppBar(),
			
			body: _buildBody()
		);
	}

	
	// DOC
	/// Build the app
	//
	Widget _buildAppBar() => CustomAppBar(
			color: Env.Colors.MainBackgroundColor,
			title: Text('List',
				style: GoogleFonts.lato()
			),
			actions: < Widget > [
				IconButton(
					icon: Icon(Icons.add),
					onPressed: () {
						return true;
					},
				),
			],
		);

	
	// DOC
	/// Build the body
	//
	Widget _buildBody() => Container(
			// padding: EdgeInsets.only(top: 0, bottom: 17),
			color: Env.Colors.MainBackgroundColor,
			child: 
			GridView.count(
				crossAxisSpacing: 5,
				crossAxisCount: 2,
				children: List.generate(
					
					20, 
					(index) => Padding(
					  padding: const EdgeInsets.only(top: 8, left:2, right: 2),
					  child: Card(
					    	color: Colors.amber,
					    	child: Text(index.toString())
					    ),
					),
					)
			)
		);

}