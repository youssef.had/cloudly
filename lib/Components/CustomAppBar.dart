import 'package:cloudly/Env/Colors.dart' as Env;
import 'package:cloudly/Env/Sizes.dart' as Env;
import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  // DOC
  /// The height from app config envirement
  double _height;

  // DOC
  /// The list of actions in appbar
  List<Widget> actions;

  // DOC
  /// The title of the app
  Widget title;

  // DOC
  /// The title of the app
  Brightness _brightness;

  // DOC
  /// The title of the app
  Color _backgroundColor;

  CustomAppBar(
      {double height,
      List<Widget> actions,
      @required this.title,
      bool isLight,
      Color color}) {
    this._height = height ?? Env.Sizes.CustomAppBarHeight;
    this.actions = actions ?? <Widget>[];
    this._backgroundColor = color ?? Env.Colors.MainBackgroundColor;
    this._brightness = isLight ?? true ? Brightness.light : Brightness.light;
  }

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(Env.Sizes.CustomAppBarHeight),
      child: AppBar(
        brightness: this._brightness,
        actionsIconTheme: IconThemeData(color: Colors.black),
        backgroundColor: _backgroundColor,
        elevation: 5,
        flexibleSpace: _getContent(),
        actions: actions,
      ),
    );
  }

  // DOC
  /// Get the content of the app bar
  //
  Widget _getContent() => Align(
      alignment: Alignment.bottomLeft,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: DefaultTextStyle(
          child: title,
          style: TextStyle(
              color: Colors.black, fontSize: 25, fontWeight: FontWeight.bold),
        ),
      ));

  @override
  // INFO: implement preferredSize
  Size get preferredSize => Size.fromHeight(_height);
}
