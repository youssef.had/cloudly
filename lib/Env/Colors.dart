

import 'package:flutter/material.dart' as Core;

class Colors {
	static const Core.Color MainColor = Core.Color(0xff262559);
	static const Core.Color MainBackgroundColor = Core.Colors.white;
	static const Core.Color MainTextColor = Core.Colors.black87;
	static const Core.Color SecondColor = Core.Color(0xff393986);
}